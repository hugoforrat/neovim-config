local ft = vim.filetype

ft.add {
	extension = {
		todo = "todo",
		mail = "mail",
		h    = "c",
		j2 = function(path)
			return ft.match({filename = string.sub(path, 1, -4)})
		end
	},
	filename = {
		Kbuild = 'make'
	},
	pattern = {
		['.*[tT][oO][dD][oO]'] = 'todo' -- case insensitive
	}
}
