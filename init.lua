vim.g.mapleader = ' '
vim.g.maplocalleader = ','

-- Bootstrap Lazy plugin manager
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git", "clone", "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	'tpope/vim-commentary',
	'tpope/vim-fugitive',
	'tpope/vim-markdown',
	'markonm/traces.vim',
	'godlygeek/tabular',
	'lervag/vimtex',
	'rbong/vim-buffest',
	'neovim/nvim-lspconfig',
	'dhruvasagar/vim-table-mode',
	'AndrewRadev/linediff.vim',
	'ellisonleao/gruvbox.nvim',
	'kylechui/nvim-surround',
	{ 'L3MON4D3/LuaSnip', version = "v2.*", build = "make install_jsregexp" },
	{ 'nvim-telescope/telescope.nvim', dependencies = {{'nvim-lua/plenary.nvim'}} },
	{ 'lewis6991/gitsigns.nvim', tag='release' },
	{
		'nvim-treesitter/nvim-treesitter',
		build = function() require('nvim-treesitter.install').update({ with_sync = true }) end
	},
}, {install = {missing = false}, change_detection = {enabled = false}})

require 'snippets'
require 'options'
require 'autocmds'
require 'maps'
require 'cmds'
require 'lsp'
require 'english'
require 'plugins'
require 'redir'

vim.cmd 'colorscheme gruvbox'

local disable_hlgroup = require 'util'.disable_hlgroup
-- Disable semantic highlighting for now
-- https://www.reddit.com/r/neovim/comments/12gvms4/this_is_why_your_higlights_look_different_in_90/
for _, group in ipairs(vim.fn.getcompletion("@lsp", "highlight")) do
	disable_hlgroup(group)
end
disable_hlgroup("DiagnosticUnnecessary")
