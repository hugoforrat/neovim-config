local set = vim.keymap.set
local fmt = string.format

require "maps.quickfix"

local function setexpr(m, lhs, rhs, opts)
	opts = opts or {}
	opts.expr = true
	set(m, lhs, rhs, opts)
end

local function esc()
	local termcode = vim.api.nvim_replace_termcodes
	local feedkeys = vim.api.nvim_feedkeys

	feedkeys(termcode('<Esc>', true, false, true), 'nx', false)
end

local function toggle(optname)
	if vim.o[optname] then vim.o[optname] = false else vim.o[optname] = true end
end

-- Mapping to visually select and perform operation _within a line_
-- 'il' => 'inside line' (without rightmost whitespace nor newline)
set({'o', 'x'}, "il", [[:<C-u>execute "normal!_vg_"<cr>]], {silent = true})
set("n", "<Leader>s", [[:%s/\<<C-r><C-w>\>/]])
set("n", "<Leader>v", [[:vimgrep "\<<C-r><C-w>\>"]])

local function navbuffer(inc)
	local setbuf = vim.api.nvim_win_set_buf
	local function wrap(index, size)
		return (index == 0 and size)
			  or (index == size + 1 and 1)
			  or index
	end

	local index
	local winid = vim.fn.win_getid()

	local bufs = vim.tbl_filter(function(buf)
		return vim.bo[buf.bufnr].buftype ~= 'quickfix'
	end, vim.fn.getbufinfo({buflisted = 1}))

	for i, buf in ipairs(bufs) do
		if vim.tbl_contains(buf.windows, winid) then
			index = i
			break
		end
	end

	if not index then
		return
	end

	index = wrap(inc and index + 1 or index - 1, #bufs)
	setbuf(0, bufs[index]['bufnr'])
end

set('n', 'L', function() navbuffer(true)  end)
set('n', 'H', function() navbuffer(false) end)

set('n', ']]', function() vim.cmd 'tabnext' end)
set('n', '[[', function() vim.cmd 'tabprevious' end)

set('n', '<C-h>', '<C-w>h')
set('n', '<C-j>', '<C-w>j')
set('n', '<C-k>', '<C-w>k')
set('n', '<C-l>', '<C-w>l')

set('n', 'gF', '<C-w><C-f><C-w>L')

set('n', 's', ':%s/')
set('n', 'S', ':s/')

-- Paste and indent what's been pasted
set('n', ']p', "p'[=']")

-- Always stays on the same column when centering the screen
set('n', 'z.', 'zz')

set('i', '<C-a>', '<Esc>A')

local nope = function() end
set('n', 'gQ', nope)
set('n', 'Q',  nope)
set('n', 'U',  nope)

set('v', 's', ':s/\\%V')

set('c', '<C-j>', '<Down>')
set('c', '<C-k>', '<Up>')

set('v', '<C-j>', 'J')
set('v', '+', '"+y')

-- Move visual block
set('v', 'J', ":m '>+1<CR>gv=gv")
set('v', 'K', ":m '<-2<CR>gv=gv")

local function hlstoggle(arg)
	local ret = not vim.o.hlsearch and ((arg and fmt('/%s<cr>', arg)) or '/')
	toggle('hlsearch')
	vim.cmd 'redraw'
	return ret
end

local visual = require 'util'.text_visual
-- Toggle Hlsearch with <C-s>
setexpr('n', '<C-s>', hlstoggle)
setexpr('n', '<Leader><C-s>', function() return hlstoggle(vim.fn.expand('<cword>')) end)
setexpr('v', '<C-s>', function()
	vim.o.hlsearch = true
	esc()
	return '/' .. visual() .. '<cr>gv<esc>'
end)

-- search for selected text with '/'
setexpr('v', '/', function()
	esc()
	return '/' .. visual()
end)

-- Wrapper around Backspace: close quickly a cmdwindow or toggle the quickfix
-- list
set('n', '<BS>', function()
	if vim.fn.getcmdwintype() ~= "" then -- In a cmd window
		vim.api.nvim_win_close(0, false)
		return
	end

	local function cmd()
		-- If a loclist/quickfix window exists, close it
		for _, win in ipairs(vim.fn.getwininfo()) do
			if win.loclist == 1 then
				return 'lclose'
			elseif win.quickfix == 1 then
				return 'cclose'
			end
		end

		if #vim.fn.getloclist(0) > 0 then
			return fmt('lopen %s| wincmd J', #vim.fn.getloclist(0) < 10 and tostring(#vim.fn.getloclist(0)) or '')
		elseif #vim.fn.getqflist() > 0 then
			return fmt('copen %s| wincmd J', #vim.fn.getqflist() < 10 and tostring(#vim.fn.getqflist()) or '')
		end
	end

	vim.cmd(cmd() or [[exe "normal! \<bs>"]])
end)
