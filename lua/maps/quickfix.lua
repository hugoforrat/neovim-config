local set = vim.keymap.set
local filter = vim.tbl_filter

local function buffilter(entry) return entry.bufnr and entry.bufnr ~= 0 end
local getqflist = vim.fn.getqflist
local getloclist = function(...) return vim.fn.getloclist(0, ...) end

local function homogeneous(up, idx, list, prefix)
	local size = #list
	local cmd
	idx, cmd = up and idx - 1 or idx + 1, up and 'prev' or 'next'
	cmd = (idx == 0 and 'last') or (idx == size + 1 and 'first') or cmd
	return prefix .. cmd
end

local function heterogeneous(up, idx, list, prefix)
	local cmd, index_first, index_last
	local fn = up and function(x) return x - 1 end or function(x) return x + 1 end

	for i, v in ipairs(list) do
		if buffilter(v) then
			if not index_first then index_first = i end
			index_last = i
		end
	end

	index_first, index_last = tostring(index_first), tostring(index_last)

	while not cmd do
		idx = fn(idx)
		if idx == 0 then
			cmd = prefix .. prefix .. ' ' .. index_last
		elseif idx == #list + 1 then
			cmd = prefix .. prefix .. ' ' .. index_first
		elseif buffilter(list[idx]) then
			cmd = up and 'prev' or 'next'
			cmd = prefix .. cmd
		end
	end

	return cmd
end

local function wrapper_error(up, get, prefix)
	local list = get()
	local filtered = filter(buffilter, get())
	local idx = get({idx = 0}).idx

	local cmd

	if #filtered == 0 or #filtered == #list then
		cmd = homogeneous(up, idx, list, prefix)
	else
		cmd = heterogeneous(up, idx, list, prefix)
	end

	vim.cmd(cmd)
end

local function pick_error(up)
	if #getqflist() > 0 then
		wrapper_error(up, getqflist, 'c')
	elseif #getloclist() > 0 then
		wrapper_error(up, getloclist, 'l')
	else
		vim.cmd("normal! " .. (up and "k" or "j"))
	end
end

set('n', '<Up>',   function() pick_error(true)  end)
set('n', '<Down>', function() pick_error(false) end)
