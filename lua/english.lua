local opt = vim.opt_local

local function setup()
	opt.spell = true
	opt.spelllang = "en"
	vim.cmd [[
	iabbrev <buffer> THe The
	iabbrev <buffer> THis This
	iabbrev <buffer> THere There
	iabbrev <buffer> THose Those
	]]
end

local filetypes = {'tex', 'markdown', 'rst', 'gitcommit'}

vim.api.nvim_create_autocmd("FileType", {
	pattern = filetypes,
	callback = setup
})

