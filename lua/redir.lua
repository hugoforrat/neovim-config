-- [[ Redirect the output of a Vim or external command into a scratch buffer.
-- Inspired by romainl's gist (https://gist.github.com/romainl/eae0a260ab9c135390c30cd370c20cd7)
-- ]]

local opt = vim.opt_local

local function emptybuffer()
	return vim.fn.line('$') == 1 and vim.fn.getline(1) == ''
end

local function redirection(cmd, param, smods)
	cmd = param and (cmd .. ' ' .. param) or cmd
	local output = vim.fn.execute(cmd)

	if not emptybuffer() then
		vim.api.nvim_cmd ({cmd = 'new', mods = smods}, {})
		vim.w.scratch = 1
	end

	opt.buftype   = 'nofile'
	opt.bufhidden = 'wipe'
	opt.buflisted = false
	opt.swapfile  = false

	vim.api.nvim_buf_set_lines(0, 0, -1, false, vim.fn.split(output, '\n'))
end

vim.api.nvim_create_user_command('Redir', function(arg)
	local param
	if arg.range == 1 then
		param = vim.fn.getline(arg.line1)
	elseif arg.range == 2 then
		param = vim.fn.join(vim.fn.getline(arg.line1, arg.line2), '\n')
	end

	redirection(arg.args, param, arg.smods)
end, {complete='command', nargs = 1, range = true})
