local ls = require("luasnip")
local snippet = ls.snippet
local t = ls.text_node
local i = ls.insert_node

local M = {}

function M.C_main()
	return snippet (
	"main", {
		t("int main(int argc, char *argv[]) {"),
		t({"", "\t"}), i(1),
		t({"", "}"})
	})
end

function M.textsnippet(trigger, text)
	if type(text) == 'string' then
		return snippet (trigger, {t(text)})
	else
		local first = true
		return snippet (trigger, vim.tbl_map(function(txt)
			if first then
				first = false
				return t(txt)
			else
				return t {"", txt}
			end
		end, text))
	end
end

return M
