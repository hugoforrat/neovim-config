local snippetpath =  './snippets'

local ls = require("luasnip")
require("luasnip.loaders.from_lua").lazy_load {paths = snippetpath}
ls.config.set_config {
	update_events = "TextChanged,TextChangedI"
}

snippetpath = vim.fs.dirname(vim.env.MYVIMRC) .. '/' .. snippetpath

local function exists(file)
	return vim.fn.filereadable(file) == 1
end

local function snippet_setup()
	vim.keymap.set({ "i", "s" }, "<Tab>", function()
		if ls.expand_or_jumpable() then
			ls.expand_or_jump()
		end
	end, { silent = true })

	vim.cmd [[
	imap <silent><expr> <C-n> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-n>'
	smap <silent><expr> <C-n> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-n>'
	]]

	-- vim.keymap.set({ "i", "s" }, "<C-n>", function()
	-- 	if ls.choice_active() then
	-- 		ls.change_choice(1)
	-- 	else
	-- 		vim.cmd([[normal!i<C-n>]])
	-- 	end
	-- end, { silent = true })

	-- vim.api.nvim_create_autocmd( "InsertLeave", {
	-- 	pattern = "*",
	-- 	callback = function()
	-- 		while ls.expand_or_jumpable() do
	-- 			ls.unlink_current()
	-- 		end
	-- 	end
	-- })
end

-- Create snippets mappings for every buffer if there is an `all.lua` snippet
-- file, otherwise create them for filetypes which have their own snippet file
if exists(snippetpath .. '/all.lua') then
	snippet_setup()
else
	local filetypes = {}
	for name, type in vim.fs.dir(snippetpath) do
		if type == 'file' then
			local fts = vim.split(name, '[%.%/]')
			local ft = fts and fts[#fts - 1]
			vim.list_extend(filetypes, {ft})
		end
	end
	-- Extras filetype for which I want snippets even without these files
	vim.list_extend(filetypes, {'lua'})
	vim.api.nvim_create_autocmd("FileType", {
		pattern = filetypes,
		callback = snippet_setup,
	})
end
