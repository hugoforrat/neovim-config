local config = require 'lspconfig'
local diag = vim.diagnostic

local function set(lhs, rhs, opts)
	opts = opts or {}
	opts.silent = true
	opts.buffer = opts.buffer or true
	vim.keymap.set('n', lhs, rhs, opts)
end

local close = function() vim.api.nvim_win_close(0, false) end

vim.api.nvim_create_autocmd("WinEnter", {
	callback = function()
		if vim.w['textDocument/hover'] then
			local bufnr = vim.fn.winbufnr(0)
			set ('K', close, {buffer = bufnr})
		end
	end
})

local mappings = {
	gD             = vim.lsp.buf.declaration,
	gd             = vim.lsp.buf.definition,
	gtd            = vim.lsp.buf.type_definition,
	K              = vim.lsp.buf.hover,
	-- TODO: gK to open documentation in scratch buffer
	-- TODO binding to highlight the reference
	gi             = vim.lsp.buf.implementation,
	gr             = vim.lsp.buf.references,
	['<Leader>rn'] = vim.lsp.buf.rename,
	['<Leader>ca'] = function() vim.lsp.buf.code_action({includeDeclaration = false}) end,
	['<Leader>gd'] = function() vim.cmd 'vsp'; vim.lsp.buf.definition() end,
	['<Leader>q']  = diag.setqflist,
	['[d']         = diag.goto_prev,
	[']d']         = diag.goto_next,
}

local servers = {
	ccls = {
		start = function()
			local dir = vim.fn.getcwd()
			return vim.fn.filereadable(dir .. '/compile_commands.json') == 1
		end
	},
	pylsp = {},
	['rust_analyzer'] = {
		start = true,
		settings = {
			['rust-analyzer'] = {}
		},
	},
	lua_ls = require("lsp.lua_ls")
}

local function backslashsetup()
	local bs = '\\'
	local open = vim.diagnostic.open_float
	set(bs, function()
		local bufnr = open()
		set(bs, close, {buffer = bufnr})
	end, {})
end

local function on_attach()
	vim.opt_local.omnifunc = 'v:lua.vim.lsp.omnifunc'
	backslashsetup()

	for lhs, rhs in pairs(mappings) do
		set(lhs, rhs)
	end
end

for server, conf in pairs(servers) do
	config[server].setup {
		on_attach = on_attach,
		autostart = type(conf.start) == 'boolean' and conf.start or false,
		settings = conf.settings,
		root_dir = conf.root_dir
	}

	if conf.start and type(conf.start) == 'function' and config[server].filetypes then
		vim.api.nvim_create_autocmd("FileType", {
			pattern = config[server].filetypes,
			callback = function(...)
				if conf.start(...) then
					vim.cmd 'LspStart'
				end
			end
		})
	end
end
