local util = require 'lspconfig.util'
local root_files = {
	'.luarc.json',
	'.luarc.jsonc',
	'.luacheckrc',
	'.stylua.toml',
	'stylua.toml',
	'selene.toml',
	'selene.yml',
}

-- Only start the Lua LSP for the nvim directory and my local plugins
local function start(arg)
	local path = require 'util'.fullpath(arg.file)

	if vim.startswith(path, vim.fs.dirname(vim.env.MYVIMRC)) then
		return true
	end

	local plugins = require "lazy.core.config".plugins
	for _, plugin in pairs(plugins) do
		if plugin._.dir and vim.startswith(path, plugin._.dir) then
			return true
		end
	end

	return false
end

return {
	start = start,
	settings = {
		Lua = {
			runtime = {version = 'LuaJIT'},
			diagnostics = {
				globals = {'vim'},
				disable = {'luadoc-miss-type-name'}
			},
			workspace = {
				checkThirdParty = false,
				library = {
					vim.env.VIMRUNTIME
				}
			},
			telemetry = {enable = false},
		},
	},
	root_dir = function(fname)
		local root = util.root_pattern(unpack(root_files))(fname)
		if root and root ~= vim.env.HOME then
			return root
		end
		return util.find_git_ancestor(fname)
	end
}
