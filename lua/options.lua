vim.opt.showcmd = true
vim.opt.incsearch = true
vim.opt.autowrite = true
vim.opt.linebreak = true
vim.opt.breakindent = true
vim.opt.wildmenu = true
vim.opt.hidden = true
vim.opt.autoread = true
vim.opt.undofile = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.autoindent = true
vim.opt.gdefault = true

-- Never use dos file format
vim.opt.fileformats = "unix"

vim.opt.modeline = false
vim.opt.showmatch = false
vim.opt.hlsearch = false

vim.opt.history = 10000
vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.laststatus = 2

vim.opt.mouse = 'n'
vim.opt.mousemodel = 'extend'
vim.opt.foldcolumn = '1'
vim.opt.showbreak = '˪'
vim.opt.background = 'dark'
vim.opt.statusline = '%m %= %f %= %y %r %{FugitiveStatusline()}'

vim.opt.completeopt = "menu"

vim.opt.listchars = {
	tab = '▸ ',
	eol = '¬',
	space = '␣'
}

vim.opt.wildmode = {
	'longest',
	'lastused',
	'full'
}

vim.opt.wildignore = {
	'*.aux', '*.log', '*.o', '*.obj'
}

-- Disable warnings for Providers I don't care about
vim.g.loaded_ruby_provider = 0
vim.g.loaded_node_provider = 0
vim.g.loaded_perl_provider = 0

-- Netrw
vim.g.netrw_liststyle = 3 -- Open netrw in tree mode
vim.g.netrw_banner    = 0 -- Remove the banner
vim.g.netrw_winsize   = 25

-- Tells vim to always expects LaTeX code (instead of plain tex code) when reading a .tex file
vim.g.tex_flavor = "latex"

vim.g.compiler_gcc_ignore_unmatched_lines = 1
