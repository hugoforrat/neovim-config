local autocmd = vim.api.nvim_create_autocmd

local KEYS = {
	DIFF_AUTOCMD  = 'diffmap_autocmd',
	SAVED_AUTOCMD = 'diffmap_saved'
}

local function getautocmd(bufnr)
	for _, cmd in ipairs(vim.api.nvim_buf_get_keymap(bufnr, 'n')) do
		if cmd.lhs == '<CR>' then
			vim.b[bufnr][KEYS.SAVED_AUTOCMD] = cmd
			return
		end
	end

	for _, cmd in ipairs(vim.api.nvim_get_keymap('n')) do
		if cmd.lhs == '<CR>' then
			vim.b[bufnr][KEYS.SAVED_AUTOCMD] = cmd
			return
		end
	end
end

local function delmap(bufnr)
	local function extract(tbl, key)
		local tmp = tbl[key]
		tbl[key] = nil
		return tmp
	end

	vim.keymap.del('n', '<cr>', {buffer = bufnr})

	if vim.b[bufnr][KEYS.SAVED_AUTOCMD] then
		local mapping = vim.b[bufnr][KEYS.SAVED_AUTOCMD]
		local mode   = extract(mapping, 'mode')
		local lhs    = extract(mapping, 'lhs')
		local rhs    = extract(mapping, 'rhs')
		local buffer = extract(mapping, 'buffer')

		mapping['lhsraw'] = nil

		mapping['sid'] = nil
		mapping['lnum'] = nil

		local set
		if buffer == 0 then
			set = vim.api.nvim_set_keymap
		else
			set = function(...) vim.api.nvim_buf_set_keymap(buffer, ...) end
		end

		set(mode, lhs, rhs, mapping)
	end
end

local function remove_others(cur_bufnr)
	for _, buf in ipairs(vim.fn.getbufinfo()) do
		if buf.bufnr ~= cur_bufnr and vim.b[buf.bufnr][KEYS.DIFF_AUTOCMD] then
			delmap(buf.bufnr)
			vim.api.nvim_del_autocmd(vim.b[buf.bufnr][KEYS.DIFF_AUTOCMD])
			vim.b[buf.bufnr][KEYS.DIFF_AUTOCMD] = nil
		end
	end
end

local function add_del_autocmd(bufnr)
	vim.b[bufnr][KEYS.DIFF_AUTOCMD] = autocmd({"BufHidden", "BufDelete"}, {
		callback = function()
			delmap(bufnr)
			vim.b[bufnr][KEYS.DIFF_AUTOCMD] = nil
			remove_others(bufnr)
			return true
		end,
		buffer = bufnr
	})
end

local function setup(bufnr)
	getautocmd(bufnr)
	vim.keymap.set('n', '<cr>', ':diffput<cr>', {buffer = bufnr})
	add_del_autocmd(bufnr)
end

local M = {}

M.OptionSet = function()
	local set = vim.v.option_new == "1"
	local bufnr = vim.fn.bufnr()

	if set then setup(bufnr) end

	local autocmdid = vim.b[bufnr][KEYS.DIFF_AUTOCMD]
	if not set and vim.v.option_oldlocal == "1" and autocmdid then
		vim.keymap.del('n', '<cr>', {buffer = true})
		vim.api.nvim_del_autocmd(autocmdid)
		remove_others(bufnr)
	end
end

M.VimEnter = function()
	for _, win in ipairs(vim.fn.getwininfo()) do
		if vim.wo[win.winid].diff then setup(win.bufnr) end
	end
end

return M
