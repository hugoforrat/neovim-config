if not vim.fn.executable("xxd") then
	return function() end
end

local autocmd = vim.api.nvim_create_autocmd

local function write_pre_cb()
	if vim.bo.binary then vim.cmd [[%!xxd -r]] end
end

local function write_post_cb()
	if vim.bo.binary then
		vim.cmd [[%!xxd -r]]
		vim.bo.modified = false
	end
end

local function binary_setup(buf)
	vim.bo[buf].filetype = 'xxd'
	vim.cmd [[%!xxd]]

	autocmd("BufWritePre",  {buffer=buf, callback=write_pre_cb})
	autocmd("BufWritePost", {buffer=buf, callback=write_post_cb})
end

local function has_null_byte(buf)
	local lines = vim.api.nvim_buf_get_lines(buf, 0, -1, false)
	for _, line in ipairs(lines) do
		if line:find('\0') then
			return true
		end
	end
end

return function(arg)
	local buf = arg.buf
	if vim.bo[buf].filetype ~= '' then
		return
	end

	if vim.bo[buf].binary then
		binary_setup(buf)
		return
	end

	if has_null_byte(buf) then
		vim.ui.input({prompt="Null byte found in the current file. Treat as binary? (Y/n): "},
		function(input)
			if input and (input == "" or input:find('^[yY]')) then
				vim.bo[buf].binary = true
				binary_setup(buf)
			end
		end)
	end
end
