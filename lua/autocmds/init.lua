local diff = require "autocmds.diff"
local binary = require "autocmds.binary"

local function autocmd(args)
	local desc, events, callback, opts = unpack(args)
	opts = opts or {}
	return vim.api.nvim_create_autocmd(events, vim.tbl_extend("error", {desc = desc, callback = callback}, opts))
end

autocmd {
	"Resize the splits when Vim is itself resized",
	"VimResized", function() vim.cmd "wincmd =" end
}

autocmd {
	 "Highlight on yank",
	 "TextYankPost", function() vim.highlight.on_yank {higroup='IncSearch', timeout=150} end
}

autocmd {
	"Create header guards for new C/C++ files",
	"BufNewFile", function()
		local filename = vim.fn.expand('%:t')
		filename = vim.fn.toupper(filename)

		local identifier = vim.fn.substitute(filename, [[\.]], "_", "g")
		identifier = vim.fn.substitute(identifier, [[-]], "_", "g")
		vim.fn.append(0, {"#ifndef " .. identifier, "#define " .. identifier})
		vim.fn.append("$", "#endif  // " .. identifier)
	end, {pattern = {'*.h', '*.hpp', '*.hh'}}
}

autocmd {
	"Enable cursorline when entering a window",
	{"WinEnter", "VimEnter"}, function() vim.wo.cursorline = true end
}

autocmd {
	 "Disable cursorline when leaving a window",
	 "WinLeave", function() vim.wo.cursorline = false end
}

autocmd {
	"Update a symlink towards the current vim server",
	{"VimEnter", "FocusGained"}, function()
		if vim.bo.filetype ~= 'man' then
			local cmd = {
				"/usr/bin/ln",  "-sf", vim.v.servername,
				vim.fn.expand("~/.local/share/nvim/last")
			}
			vim.fn.jobstart(cmd, {detach=true})
		end
	end
}

autocmd {
	"Set/unset the diffput mapping depending on whether the diff option is set",
	"OptionSet", diff.OptionSet, {pattern = "diff"}
}

autocmd {
	"Set the diffput mapping if nvim was started with the diff option (i.e nvim -d/vimdiff)",
	"VimEnter", diff.VimEnter
}

autocmd {
	"Example given in :h using-xxd, extended for arbitrary binary files",
	"BufWinEnter", binary
}
