local telescope = require 'telescope'
local builtin = require 'telescope.builtin'

telescope.setup {
	defaults = {
		file_ignore_patterns = {"^out/", "%.jpg", "%.jpeg", "%.png", "%.pdf", "%.bib"},
		mappings = {
			i = {
				["<C-j>"] = "move_selection_next",
				["<C-k>"] = "move_selection_previous"
			}
		}
	}
}
-- Telescope remappings
vim.keymap.set('n', '<C-f>', builtin.find_files)
vim.keymap.set('n', '<C-g>', builtin.live_grep)
vim.keymap.set('n', '<C-b>', builtin.lsp_dynamic_workspace_symbols)
