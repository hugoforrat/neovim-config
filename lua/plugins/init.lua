require "plugins.telescope"
require "plugins.surround"

local gitsigns = require "gitsigns"
gitsigns.setup {
	on_attach = function()
		vim.keymap.set('n', 'gn', function() gitsigns.next_hunk({navigation_message=false}) end)
		vim.keymap.set('n', 'gN', function() gitsigns.prev_hunk({navigation_message=false}) end)
	end
}


-- VimTeX options
vim.g.vimtex_view_method = 'zathura'
vim.g.vimtex_compiler_latexmk = {
	out_dir = 'out',
	callback = 1,
	continuous = 1,
	executable = 'latexmk',
	hooks = {},
	options = {
		'-verbose',
		'-file-line-error',
		'-synctex=1',
		'-interaction=nonstopmode',
	}
}

-- Tree sitter
-- require'nvim-treesitter.configs'.setup {
--   -- A list of parser names, or "all"
--   ensure_installed = { "c", "cpp", "lua" },

--   -- Install parsers synchronously (only applied to `ensure_installed`)
--   sync_install = false,

--   -- Automatically install missing parsers when entering buffer
--   -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
--   auto_install = false,

--   -- List of parsers to ignore installing (for "all")
--   ignore_install = { "latex" , "javascript" },

--   ---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
--   -- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

--   highlight = {
--     -- `false` will disable the whole extension
--     enable = false,

--     -- HUGO: Cool parce que permet de donner la bonne couleur aux typedef definis par moi meme mais horrible couleur pour toutes les fonctions

--     -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
--     -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
--     -- the name of the parser)
--     -- list of language that will be disabled
--     disable = { "help" },
--     -- -- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files

--     -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
--     -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
--     -- Using this option may slow down your editor, and you may see some duplicate highlights.
--     -- Instead of true it can also be a list of languages
--     additional_vim_regex_highlighting = false,
--   },
-- }

