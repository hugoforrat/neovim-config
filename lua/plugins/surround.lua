require "nvim-surround".setup ()

local M = {}

-- sl => single line
M.surround_sl = function(handle, lh, rh)
	local config = require "nvim-surround.config"
	local get_selection = config.get_selection
	local get_selections = config.get_selections
	local default_aliases = config.default_opts.aliases
	local setup = require "nvim-surround".buffer_setup

	local surround = {
		[handle] = {
			add = function()
				return { {lh}, {rh} }
			end,
			find = function()
				return get_selection { pattern = string.format('%s.-%s', vim.pesc(lh), vim.pesc(rh))}
			end,
			delete = function()
				return get_selections {char = handle, pattern = string.format('^(%s)().-(%s)()$', vim.pesc(lh), vim.pesc(rh))}
			end
		}
	}

	local aliases
	if default_aliases[handle] then
		aliases = {
			[handle] = false
		}
	end

	setup {
		surrounds = surround,
		aliases = aliases
	}
end

return M
