local getpos = vim.fn.getpos
local function gettext(a,b,c,d) return vim.api.nvim_buf_get_text(0,a-1,b-1,c-1,d,{}) end

local M = {}

M.text_visual = function()
	if vim.fn.mode() == 'v' then
		local _, l_cur, c_cur, _ = unpack(getpos("."))
		local _, l_end, c_end, _ = unpack(getpos("v"))
		local c_min, c_max, l_min, l_max

		if l_cur == l_end then -- Single line
			if c_cur < c_end then
				c_min, c_max = c_cur, c_end
			else
				c_min, c_max = c_end, c_cur
			end

			return gettext(l_cur, c_min, l_cur, c_max)[1]
		else
			if l_cur < l_end then
				l_min, c_min, l_max, c_max = l_cur, c_cur, l_end, c_end
			else
				l_min, c_min, l_max, c_max = l_end, c_end, l_cur, c_cur
			end

			return vim.fn.join(gettext(l_min, c_min, l_max, c_max), '\n')
		end
	else
		local _, l_beg, c_beg, _ = unpack(getpos("'<"))
		local _, l_end, c_end, _ = unpack(getpos("'>"))
		local text = gettext(l_beg, c_beg, l_end, c_end)
		return #text == 1 and text[1] or vim.fn.join(text, '\n')
	end
end

M.fullpath = function(p)
	return vim.fn.fnamemodify(p, ':p')
end


M.is_executable = function(file)
	file = vim.fn.fnamemodify(file, ":p")
	local perm = vim.fn.getfperm(file)
	return perm:match('..x......')
end

M.disable_hlgroup = function(group, namespace)
	namespace = namespace or 0
	vim.api.nvim_set_hl(namespace, group, {})
end

return M
