local cmd = vim.api.nvim_create_user_command

cmd("New", function(args)
	local vertical = args.smods.vertical
	vim.cmd((vertical and "vertical " or "") .. "new")
	vim.bo.buftype   = "nofile"
	vim.bo.bufhidden = "wipe"
	vim.bo.buflisted = false
	vim.bo.swapfile  = false
end, {})

cmd('Shebang', function()
	local shebangs = {
		python = '#!/usr/bin/env python3',
		sh     = {"#!/usr/bin/sh", '#!/usr/bin/bash'},
		zsh    = '#!/usr/bin/zsh',
		c      = '#!/usr/bin/tcc -run'
	}
	local shebang = shebangs[vim.opt.ft:get()]
	if type(shebang) == 'table' then
		vim.ui.select(shebang, {
			prompt = 'Select the shebang to use:'
		}, function(choice)
			vim.fn.append(0, choice)
		end)
	else
		vim.fn.append(0, shebang)
	end
end, {})

cmd('Cempty', function() vim.fn.setqflist({}) end, {})
