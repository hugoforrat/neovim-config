local ls = require("luasnip")
local snippet = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local C_main_snippet = require 'snippets.util'.C_main

return {
	C_main_snippet(),
	snippet (
		"printf", {
		t("printf(\""), i(1),
		t([[\n");]])
	})
}
