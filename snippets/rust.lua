local ls = require("luasnip")
local snippet = ls.snippet
local t = ls.text_node

local main = snippet ("main", {
	t("fn main() {"),
	t({"", '    println!("Hello, world!");'}),
	t({"", "}"})
})

return {
	main
}
