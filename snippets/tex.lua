local ls = require("luasnip")
local snippet = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local c = ls.choice_node
local d = ls.dynamic_node
local fmta = require("luasnip.extras.fmt").fmta

local extend = vim.list_extend

local function surroundsnippet(trigger, lhs, rhs)
	return snippet (trigger, { t(lhs), i(1), t(rhs)})
end

local function cmdsnippet(trigger, cmd)
	return surroundsnippet(trigger, string.format("\\%s{", cmd), "}")
end

local function beginsnippet()
	local fmt = [[\begin{<insert>}
<body>
\end{<dynamic>}]]
	return snippet ({trig = [[beg\(i\|in\)\?]], trigEngine = 'vim'}, fmta(fmt, {
		insert  = i(1),
		body    = i(0),
		dynamic = d(2, function(args) return sn(nil, { t(args[1]) }) end, {1}),
	}))
end

local snippets = {
	cmdsnippet('tt',  'texttt'),
	cmdsnippet('i',   'textit'),
	cmdsnippet('b',   'textbf'),
	cmdsnippet('url', 'url'),

	surroundsnippet('"', "``", "''"),
	beginsnippet(),
}

extend(snippets, {
	snippet (
		"e", {
			c(1, {
				sn(nil, {t("\\emph{"), i(1), t("}")}),
				sn(nil, {t("\\("), i(1), t("\\)")}),
			})
		}
	),
	snippet (
		"list", {
			t("\\begin{itemize}"),
			t({"", "\t\\item "}), i(1),
			t({"", "\\end{itemize}"})
		}
	),
})

local function newline()
	return t({'', ''})
end

local label = function(args, _, _, prefix)
	local txt = args[1][1]
	txt = vim.fn.tolower(txt)
	txt = vim.fn.substitute(txt, ' ', '_', 'g')
	txt = vim.fn.substitute(txt, '\\W', '', 'g')
	return sn(nil, {
		t('\\label{' .. prefix .. ':' .. txt),
		i(1),
		t('}')
	})
end

local emptylabel = function(_, _, _, prefix)
	return sn(nil, {
		t('\\label{' .. prefix .. ':'),
		i(1),
		t('}')
	})
end

local sections = {
	sec = 'section',
	ssec = 'subsection',
	sssec = 'subsubsection'
}

for prefix, name in pairs(sections) do
	extend(snippets, {
		snippet (
		prefix, {
			t("\\" .. name .. "{"), i(1), t("}"),
			c(2, {
				d(nil,      label, {1}, {user_args={prefix}}),
				d(nil, emptylabel, {1}, {user_args={prefix}}),
				i(nil)
			}),
			newline(), i(3)
		})
	})
end

return snippets
