local ls = require("luasnip")
local snippet = ls.snippet
local i = ls.insert_node
local f = ls.function_node
local fmt = require("luasnip.extras.fmt").fmt

local C_main_snippet = require 'snippets.util'.C_main

local function Cpp_auto_for()
	local function get_container(_, parent)
		local container = parent.captures[1]
		return container
	end
	local function get_first_char(_, parent)
		local container = parent.captures[1]
		return container:sub(1, 1)
	end
	local format =
[[for (const auto& {first_char} : {container}) {{
		{insert}
}}]]
	return snippet ({ trig = "for%s+(%S%S+)", trigEngine = "pattern" },
	fmt(format, {
		first_char = f(get_first_char, nil, {}),
		container = f(get_container, nil, {}),
		insert = i(0)
	}))
end

return {
	C_main_snippet(),
	Cpp_auto_for(),
}
