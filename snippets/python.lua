local textsnippet = require "snippets.util".textsnippet

return {
	textsnippet ( "main", {
		"if __name__ == '__main__':",
		"\tmain()"
	}),
	textsnippet ( "numpy", "import numpy as np"),
	textsnippet ( "fig", "fig, ax = plt.subplots()"),
	textsnippet ( "plt", "import matplotlib.pyplot as plt")
}
