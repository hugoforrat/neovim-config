local is_executable = require "util".is_executable

if is_executable(vim.fn.bufname()) then
	vim.keymap.set('n', '<cr>', ':!%:p<cr>', {buffer = true})
end
