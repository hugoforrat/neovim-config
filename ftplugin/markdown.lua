local surroundsetup = require "nvim-surround".buffer_setup

-- TODO: 'gc' surround for comments <!-- xxx -->

surroundsetup {
	surrounds = {
		['c'] = {
			add = function(_, line_mode)
				return line_mode and { {"```"}, {"```"} } or { {"`"}, {"`"} }
			end,
		}
	}
}
