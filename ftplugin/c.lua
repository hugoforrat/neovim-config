local opt = vim.opt_local

local surround = function(arg)
	require "nvim-surround".buffer_setup {
		surrounds = arg
	}
end

opt.shiftwidth  = 0
opt.tabstop     = 8
opt.softtabstop = 4

vim.api.nvim_buf_create_user_command(0, 'Kerneldoc', function() require("tsdoc")() end, {})

local locks = {
	{"Bottom half lock (spin_[un]lock_bh)", {"spin_lock_bh();"},  {"spin_unlock_bh();"}},
	{"RCU lock (rcu_read_[un]lock)",        {"rcu_read_lock();"}, {"rcu_read_unlock();"}}
}

local _prompts = {}
local _returns = {}

for i, v in ipairs(locks) do
	_prompts[i] = string.format("%d. %s", i, v[1])
	_returns[i] = {v[2], v[3]}
end

surround {
	['l'] = {
		add = function() return _returns[vim.fn.inputlist(_prompts)] end
	}
}
