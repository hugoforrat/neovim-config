function! ItemIfEnv()
  let l:env_found = 0
  for l:line in reverse(getline(1, line(".")))
    if l:line =~ '^\s*\\end{itemize}' || l:line =~ '^\s*\\end{enumerate}' || l:line =~ '^\s*\\end{description}'
      break
    elseif l:line =~ '^\s*\\begin{itemize}' || l:line =~ '^\s*\\begin{enumerate}' || l:line =~ '^\s*\\begin{description}'
      let l:env_found = 1
      break
    endif
  endfor
  return l:env_found
endfunction

inoremap <buffer> <expr> <cr> ItemIfEnv() ? "\n\\item " : "\n"
inoremap <buffer> <S-cr> <cr>
nnoremap <buffer> <expr> o ItemIfEnv() ? "o\\item <Esc>==A" : "o"

" Make a visual line selection into an itemize environment; each line being
" an item
vnoremap <buffer> <C-l> _`<O\begin{itemize}`>o\end{itemize}gv:normal!I\item 

" Remap 'I' so that it starts after '\item'
nnoremap <buffer> <expr> I getline('.') =~ '^\s*\\item' ? "_Wi" : "I"
