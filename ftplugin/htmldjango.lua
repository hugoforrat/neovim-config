local opt = vim.opt_local

vim.cmd 'runtime! ftplugin/html.vim'
vim.cmd 'runtime! ftplugin/django.vim'

opt.expandtab  = true
opt.shiftwidth = 4
opt.tabstop    = 4
opt.commentstring="{# %s #}"

vim.b.did_ftplugin = 1
