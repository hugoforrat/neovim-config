local surround = require "plugins.surround".surround_sl

local handle_texsize = function(action)
	local sizes = { "tiny", "scriptsize", "footnotesize", "small", "normalsize", "large", "Large", "LARGE", "huge", "Huge"}

	local actions = {
		inc={"<C-a>", function(x) return x + 1 end},
		dec={"<C-x>", function(x) return x - 1 end}
	}

	local contains = function(word)
		for index, size in ipairs(sizes) do
			if word == size then
				return index
			end
		end
		return nil
	end

	local fallback, fn = unpack(actions[action])
	local index = contains(vim.fn.expand('<cword>'))

	if not index then
		vim.cmd([[execute "normal!\]] .. fallback .. [["]])
		return
	end

	index = fn(index)

	if index == 0 then
		index = #sizes
	elseif index == #sizes + 1 then
		index = 1
	end

	vim.cmd([[execute "normal!ciw]] .. sizes[index] .. [["]])
end

vim.keymap.set('n', '<C-a>', function() handle_texsize('inc') end, {buffer = true})
vim.keymap.set('n', '<C-x>', function() handle_texsize('dec') end, {buffer = true})

surround('b', '\\textbf{', '}')
surround('"', '``', "''")
surround('e', [[\(]], [[\)]])
