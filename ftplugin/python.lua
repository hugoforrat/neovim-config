local set = vim.keymap.set
local put = vim.api.nvim_put

local FMT = [[%sprint(f"{%s=}")]]

local function cword()
	return vim.fn.expand('<cWORD>')
end

local function getline()
	local _, ln, _, _ = unpack(vim.fn.getpos("."))
	return ln
end

local function print_debug()
	local indent = vim.fn.indent(getline())

	put({string.format(FMT, string.rep(" ", indent), cword())}, "l", true, false)
	vim.fn.execute("normal!==")
end

set('n', '<Space>d', print_debug, {
	buffer=true,
	desc="Debug command",
})
