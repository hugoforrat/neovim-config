local opt = vim.opt_local
local surround = require "plugins.surround".surround_sl

local function set(mode, lhs, rhs, opts)
	opts = opts or {}
	opts.buffer = true
	vim.keymap.set(mode, lhs, rhs, opts)
end

opt.spell = true;
opt.number = false;
opt.relativenumber = false;
opt.foldcolumn = '0';
opt.textwidth = 80;

vim.cmd [[
syntax match Comment "^\s*#.*$"
syntax match GruvboxGreen "^\s*##.*$"
iabbrev <buffer> -- —
]]

set('n', 'j', 'gj')
set('n', 'k', 'gk')
set('n', '<cr>', 'vipgq')

-- Language selection logic
local langs = {"en", "fr", "de"}
local fparts = vim.split(vim.fn.expand('<afile>:t'), '%.')
local langext = #fparts > 2 and fparts[#fparts - 1]

if langext and vim.tbl_contains(langs, langext) then
	opt.spelllang = langext
else
	-- if no language "extension", we set spelllang to all possible
	opt.spelllang = langs
end

surround ('"', '“', '”')
